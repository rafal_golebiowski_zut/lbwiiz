import numpy as np
import itertools

import FuzzyNumber
import CometRule

from scipy.stats import rankdata


class Comet:
    def __init__(self, fuzzyNumbersSets):
        self.fuzzyNumbersSets = fuzzyNumbersSets
    
    def generateCharacteristicObjects(self):
        tmp = [fuzzyNumberSet.fuzzyNumbers for fuzzyNumberSet in self.fuzzyNumbersSets]
        self.characteristicObjects = list(itertools.product(*tmp))
        
    def setMEJ(self, MEJ):
        if(MEJ.shape[0] != len(self.characteristicObjects)):
            print("MEJ size doesn't match characteristicObjectsCount")
            return
        self.MEJ = MEJ
    
    def generateRuleset(self):
        SJ = np.sum(self.MEJ, axis = 1)
        k = np.unique(SJ).shape[0]
        delta = 1/(k - 1)

        P = (rankdata(SJ, 'dense') - 1) * delta

        rules = np.empty(len(self.characteristicObjects), CometRule.Rule)
        for i, characteristicObject in enumerate(self.characteristicObjects):
            rules[i] = CometRule.Rule(characteristicObject, FuzzyNumber.SingletonFuzzyNumber(P[i]))

        self.ruleset = CometRule.Ruleset(rules)

    def calculateValue(self, inputs):
        return self.ruleset.calculateValue(inputs)
