import numpy as np

import FuzzyNumber
import CometRule
import CometUtils
import Comet



fuzzyNumberSets = CometUtils.loadFuzzyNumbersSets('criteria.csv')


comet = Comet.Comet(fuzzyNumberSets)
comet.generateCharacteristicObjects()

MEJ = CometUtils.generateMEJBasedOnWeightedSum(comet, np.array([-0.21, -0.14]), 0.05)
CometUtils.pickleMEJ(MEJ)
MEJ = CometUtils.unpickleMEJ()
comet.setMEJ(MEJ)

comet.generateRuleset()

comet.ruleset.printRules()

print(comet.calculateValue(np.array([21000, 9800])))
print(comet.calculateValue(np.array([21000, 10100])))
print(comet.calculateValue(np.array([21000, 11400])))
print(comet.calculateValue(np.array([86000, 22000])))
print(comet.calculateValue(np.array([29000, 15000])))
print(comet.calculateValue(np.array([86000, 15000])))
print(comet.calculateValue(np.array([86000, 15000])))

