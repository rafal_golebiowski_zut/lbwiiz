import numpy as np
import matplotlib.pyplot as plt


class FuzzyNumber:
    def calculateValue(self, x) -> float:
        pass

class TriangularFuzzyNumber(FuzzyNumber):
    def __init__(self, a, m, b):
        self.a = a
        self.m = m
        self.b = b
        
    def calculateValue(self, x):
        if(x == self.m):
            return 1
        elif(x <= self.a):
            return 0
        elif(x <= self.m):
            return (x - self.a)/(self.m - self.a)
        elif(x <= self.b):
            return (self.b - x)/(self.b - self.m)
        elif(x >= self.b):
            return 0

class SingletonFuzzyNumber(FuzzyNumber):
    def __init__(self, m):
        self.m = m
        
    def calculateValue(self, x):
        if(x == self.m):
            return 1
        return 0

class FuzzyNumberSet:
    def __init__(self, fuzzyNumbers, name):
        self.fuzzyNumbers = fuzzyNumbers
        self.name = name
    
    def plot(self):
        xMin = min(self.fuzzyNumbers, key = lambda x: x.a).a
        xMax = max(self.fuzzyNumbers, key = lambda x: x.b).b
        for fuzzyNumber in self.fuzzyNumbers:
            plt.plot([xMin, fuzzyNumber.a, fuzzyNumber.m, fuzzyNumber.b, xMax], [0, 0, 1, 0, 0])
        plt.title(self.name)
        plt.show()
    
