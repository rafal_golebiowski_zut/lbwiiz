import numpy as np

import FuzzyNumber
import CometRule
import CometUtils
import Comet



fuzzyNumberSets = CometUtils.loadFuzzyNumbersSets('criteriaFull.csv')

# for fuzzyNumberSet in fuzzyNumberSets:
#     fuzzyNumberSet.plot()

comet = Comet.Comet(fuzzyNumberSets)
comet.generateCharacteristicObjects()

# MEJ = CometUtils.generateMEJBasedOnWeightedSum(comet, np.array([-0.21, -0.14, -0.35, -0.075, 0.075, 0.075, 0.075]), 0.05)
# CometUtils.pickleMEJ(MEJ)
MEJ = CometUtils.unpickleMEJ()
comet.setMEJ(MEJ)

comet.generateRuleset()

for i in range(10):
    comet.ruleset.rules[i].printMe()

print(comet.calculateValue(np.array([21000, 2500, 9800, 167.5, 0, 0, 0])))
print(comet.calculateValue(np.array([21000, 4000, 10100, 161.2, 3, 3, 2])))
print(comet.calculateValue(np.array([21000, 7000, 11400, 83.8, 3, 3, 2])))
print(comet.calculateValue(np.array([86000, 20000, 22000, 95.4, 1, 1, 1])))
print(comet.calculateValue(np.array([29000, 4000, 15000, 158.2, 1, 0, 0])))
print(comet.calculateValue(np.array([86000, 7000, 15000, 105.8, 1, 0, 0])))
print(comet.calculateValue(np.array([86000, 17500, 15000, 72.9, 1, 1, 1])))

