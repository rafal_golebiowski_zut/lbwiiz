import numpy as np

import FuzzyNumber
import CometRule
import CometUtils
import Comet

fuzzyNumberSet = CometUtils.loadFuzzyNumbersSets('criteriaReduced.csv')

comet = Comet.Comet(fuzzyNumberSet)
comet.generateCharacteristicObjects()

MEJ = CometUtils.askForMEJ(comet, True)
comet.setMEJ(MEJ)

comet.generateRuleset()

comet.ruleset.printRules()

print(comet.calculateValue(np.array([25000, 12000])))

