import numpy as np
import csv
import random
import math
import pickle

import FuzzyNumber

def loadFuzzyNumbersSets(path):
    with open(path, encoding = "UTF-8") as csvFile:
        reader = csv.reader(csvFile, delimiter=';')
        fuzzyNumbersSets = []
        for _, row in enumerate(reader):
            cores = np.array([float(row[index]) for index in range(1, len(row))]) 
            if(len(cores) == 1):
                fuzzyNumbers = [FuzzyNumber.TriangularFuzzyNumber(cores[0], cores[0], cores[0])]
            else:
                fuzzyNumbers = np.empty(len(cores), dtype = FuzzyNumber.TriangularFuzzyNumber)
                fuzzyNumbers[0] = FuzzyNumber.TriangularFuzzyNumber(cores[0], cores[0], cores[1])
                for j in range(1, len(cores) - 1):
                    fuzzyNumbers[j] = FuzzyNumber.TriangularFuzzyNumber(cores[j - 1], cores[j], cores[j + 1])
                fuzzyNumbers[len(cores) - 1] = FuzzyNumber.TriangularFuzzyNumber(cores[-2], cores[-1], cores[-1])

            fuzzyNumbersSets.append(FuzzyNumber.FuzzyNumberSet(fuzzyNumbers, row[0]))
    return fuzzyNumbersSets

def loadMEJ(path):
    MEJ = []
    with open(path) as csvFile:
        reader = csv.reader(csvFile, delimiter=';')
        MEJ = []

        for _, row in enumerate(reader):
            MEJ.append(np.array([float(cell) for _, cell in enumerate(row)]))
    MEJ = np.array(MEJ)

    checkMEJ(MEJ)

    return MEJ

def askForMEJ(comet, shuffle = False):
    size = len(comet.characteristicObjects)
    MEJ = np.ones((size, size)) * 0.5

    pairs = []
    for i in range(size):
        for j in range(i + 1, size):
            pairs.append([i, j])
    if(shuffle):
        random.shuffle(pairs)
        for pair in pairs:
            if random.randint(0, 1) == 1:
                pair.append(pair.pop(0))

    print("1 - CO1 wins\n2 - draw\n3 - CO2 wins")
    print("{0:<40} {1:>15}    {2:>15}".format("", "CO1", "CO2"))
    for [i, j] in pairs:
        ans = 0
        while(ans not in range(1, 4)):
            for k, fuzzyNumberSet in enumerate(comet.fuzzyNumbersSets):
                print('{0:<40} {1:>15} vs {2:>15}'.format(fuzzyNumberSet.name, 
                    comet.characteristicObjects[i][k].m, 
                    comet.characteristicObjects[j][k].m))
            
            try:
                ans = int(input())
            except ValueError:
                ans = 0    

                
        MEJ[j, i] = (ans - 1) * 0.5 
        MEJ[i, j] = 1 - MEJ[j, i]

    checkMEJ(MEJ)

    return MEJ

def generateRandomMej(size):
    MEJ = np.ones((size, size)) * 0.5
    for i in range(size):
        MEJ[i, i + 1:size] = np.random.randint(3, size = size - i - 1) * 0.5
        MEJ[i + 1:size, i] = 1 - MEJ[i, i + 1:size]

    checkMEJ(MEJ)
    
    return MEJ

def generateMEJBasedOnWeightedSum(comet, weights, eps):
    size = len(comet.characteristicObjects)
    MEJ = np.ones((size, size)) * 0.5
    
    maxs = []
    for _, fuzzyNumberSet in enumerate(comet.fuzzyNumbersSets):
        cores = list(map(lambda x: x.m, fuzzyNumberSet.fuzzyNumbers))
        maxs.append(np.max(cores))
    maxs = np.array(maxs)

    characteristicObjectsValues = []
    for i in range(MEJ.shape[0]):
        characteristicObjectCores = np.array(list(map(lambda x: x.m, comet.characteristicObjects[i])))
        characteristicObjectsValues.append(np.sum(characteristicObjectCores/maxs * weights))
    characteristicObjectsValues = np.array(characteristicObjectsValues)

    for i in range(size):
        for j in range(i + 1, size):
            a = characteristicObjectsValues[i]
            b = characteristicObjectsValues[j]
            if(math.fabs(a - b) < eps):
                MEJ[i, j] = 0.5
                MEJ[j, i] = 0.5 
            elif(a > b):
                MEJ[i, j] = 1
                MEJ[j, i] = 0
            else:
                MEJ[i, j] = 0
                MEJ[j, i] = 1

    return MEJ

def pickleMEJ(MEJ):
    file = open('MEJ', 'wb')
    pickle.dump(MEJ, file)
    file.close()

def unpickleMEJ():
    file = open('MEJ', 'rb')
    MEJ = pickle.load(file)
    file.close()
    return MEJ

def checkMEJ(MEJ):
    for i in range(MEJ.shape[0]):
        for j in range(i, MEJ.shape[0]):
            if(MEJ[i, j] + MEJ[j, i] != 1):
                print("Error in MEJ at position: (", i, ", ", j, ")")
