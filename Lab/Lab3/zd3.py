import numpy as np

import FuzzyNumber
import CometRule
import CometUtils
import Comet

fuzzyNumberSet = CometUtils.loadFuzzyNumbersSets('criteria.csv')
MEJ = CometUtils.loadMEJ('MEJ.csv')

comet = Comet.Comet(fuzzyNumberSet)
comet.generateCharacteristicObjects()
comet.setMEJ(MEJ)

comet.generateRuleset()

comet.ruleset.printRules()

print(comet.calculateValue(np.array([40000, 20000])))

