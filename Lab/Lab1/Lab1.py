import numpy as np
import matplotlib.pyplot as plt
from math import pow


class FuzzyNumber:
    def calculateValue(self, x):
        pass
    def plotMe(self, xmin, xmax):
        X = np.linspace(xmin, xmax, num=(xmax-xmin) * 10 + 1)
        Y = list(map(self.calculateValue, X))
        plt.plot(X, Y)  

class TriangleFuzzyNumber(FuzzyNumber):
    def __init__(self, a, b, m):
        self.a = a
        self.b = b
        self.m = m
        
    def calculateValue(self, x):
        if(x <= self.a):
            return 0
        elif(x <= self.m):
            return (x - self.a)/(self.m - self.a)
        elif(x == self.m):
            return 1
        elif(x <= self.b):
            return (self.b - x)/(self.b - self.m)
        elif(x >= self.b):
            return 0
    
class PiFuzzyNumber(FuzzyNumber):
    def __init__(self, a, b, m):
        self.a = a
        self.b = b
        self.m = m
        self.beta1 = (a + m)/2
        self.beta2 = (m + b)/2
        
    def calculateValue(self, x):
        if(x <= self.a):
            return 0
        elif(x <= self.beta1):
            return 2 * pow((x - self.a)/(self.m - self.a), 2)
        elif(x <= self.m):
            return 1 - 2 * pow((x - self.m)/(self.m - self.a), 2)
        elif(x == self.m):
            return 1
        elif(x <= self.beta2):
            return 1 - 2 * pow((x - self.m)/(self.b - self.m), 2)
        elif(x <= self.b):
            return 2 * pow((x - self.b)/(self.b - self.m), 2)
        elif(x >= self.b):
            return 0

class HamacherSNormFuzzyNumber(FuzzyNumber):
    def __init__(self, aFuzzy : FuzzyNumber, bFuzzy : FuzzyNumber, gamma):
        self.aFuzzy = aFuzzy
        self.bFuzzy = bFuzzy
        self.gamma = gamma
        
    def calculateValue(self, x):
        a = self.aFuzzy.calculateValue(x)
        b = self.bFuzzy.calculateValue(x)
        return (a + b + (self.gamma - 1) * a * b)/(1 + self.gamma * a * b)        
    
        
class YagerSNormFuzzyNumber(FuzzyNumber):
    def __init__(self, aFuzzy : FuzzyNumber, bFuzzy : FuzzyNumber, p):
        self.aFuzzy = aFuzzy
        self.bFuzzy = bFuzzy
        self.p = p
    
    def calculateValue(self, x):
        a = self.aFuzzy.calculateValue(x)
        b = self.bFuzzy.calculateValue(x)
        p = self.p
        return min(1, (a**p + b**p)**(1/p))

class HamacherTNormFuzzyNumber(FuzzyNumber):
    def __init__(self, aFuzzy : FuzzyNumber, bFuzzy : FuzzyNumber, gamma):
        self.aFuzzy = aFuzzy
        self.bFuzzy = bFuzzy
        self.gamma = gamma
    def calculateValue(self, x):
        a = self.aFuzzy.calculateValue(x)
        b = self.bFuzzy.calculateValue(x)
        return (a * b)/(self.gamma + (1 - self.gamma) * (a + b - a * b))
        
        
class YagerTNormFuzzyNumber(FuzzyNumber):
    def __init__(self, aFuzzy : FuzzyNumber, bFuzzy : FuzzyNumber, p):
        self.aFuzzy = aFuzzy
        self.bFuzzy = bFuzzy
        self.p = p
    
    def calculateValue(self, x):
        a = self.aFuzzy.calculateValue(x)
        b = self.bFuzzy.calculateValue(x)
        p = self.p
        return 1 - min(1, ((1 - a)**p + (1 - b)**p)**(1/p))
        
        

a = TriangleFuzzyNumber(0, 10, 5)
b = PiFuzzyNumber(5, 15, 10)
HamacherSNorm = HamacherSNormFuzzyNumber(a, b, 2)
YagerSNorm = YagerSNormFuzzyNumber(a, b, 2)
HamacherTNorm = HamacherTNormFuzzyNumber(a, b, 2)
YagerTNorm = YagerTNormFuzzyNumber(a, b, 2)

xmin = -2
xmax = 18


plt.subplot(3, 3, 1)
a.plotMe(xmin, xmax)
plt.title("a")
plt.subplot(3, 3, 2)
b.plotMe(xmin, xmax)
plt.title("b")

plt.subplot(3, 3, 3)
a.plotMe(xmin, xmax)
b.plotMe(xmin, xmax)
plt.title("a, b")

plt.subplot(3, 3, 4)
a.plotMe(xmin, xmax)
b.plotMe(xmin, xmax)
HamacherSNorm.plotMe(xmin, xmax)
plt.title("HamacherSNorm(a, b, gamma = 2)")


plt.subplot(3, 3, 5)
a.plotMe(xmin, xmax)
b.plotMe(xmin, xmax)
YagerSNorm.plotMe(xmin, xmax)
plt.title("YagerSNorm(a, b, p = 2)")

plt.subplot(3, 3, 6)
a.plotMe(xmin, xmax)
b.plotMe(xmin, xmax)
HamacherTNorm.plotMe(xmin, xmax)
plt.title("HamacherTNorm(a, b, gamma = 2)")

plt.subplot(3, 3, 7)
a.plotMe(xmin, xmax)
b.plotMe(xmin, xmax)
YagerTNorm.plotMe(xmin, xmax)
plt.title("YagerTNorm(a, b, p = 2)")

plt.subplot(3, 3, 9)
HamacherSNorm.plotMe(xmin, xmax)
YagerSNorm.plotMe(xmin, xmax)
HamacherTNorm.plotMe(xmin, xmax)
YagerTNorm.plotMe(xmin, xmax)
plt.title("AllNorms")

plt.show()



gammas = [-1, 0, 1, 5, 10, 20, 50]
for gamma in gammas:
    HamacherSNorm.gamma = gamma
    HamacherSNorm.plotMe(xmin, xmax)
plt.legend(gammas)
a.plotMe(xmin, xmax)
b.plotMe(xmin, xmax)
plt.title('HamacherSNorm')
plt.show()

ps = [1, 2, 5, 10, 20]
for p in ps:
    YagerSNorm.p = p
    YagerSNorm.plotMe(xmin, xmax)
plt.legend(ps)
a.plotMe(xmin, xmax)
b.plotMe(xmin, xmax)
plt.title('YagerSNorm')
plt.show()


gammas = [0.1, 1, 5, 10, 20, 50]
for gamma in gammas:
    HamacherTNorm.gamma = gamma
    HamacherTNorm.plotMe(xmin, xmax)
plt.legend(gammas)
a.plotMe(xmin, xmax)
b.plotMe(xmin, xmax)
plt.title('HamacherTNorm')
plt.show()


ps = [1, 2, 5, 10, 20]
for p in ps:
    YagerTNorm.p = p
    YagerTNorm.plotMe(xmin, xmax)
plt.legend(ps)
a.plotMe(xmin, xmax)
b.plotMe(xmin, xmax)
plt.title('YagerTNorm')
plt.show()

