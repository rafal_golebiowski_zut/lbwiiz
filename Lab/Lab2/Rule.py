import numpy as np
import matplotlib.pyplot as plt
import FuzzyNumber

class Rule:
    def __init__(self, fuzzyNumbers, operators, outFuzzyNumber : FuzzyNumber.FuzzyNumber):
        self.fuzzyNumbers = fuzzyNumbers
        self.operators = operators
        self.outFuzzyNumber = outFuzzyNumber
        
    def calculateValue(self, inputs, implication = FuzzyNumber.MinFuzzyNumber, plotRanges = None, plotRows = 1, plotRow = 1):
        if(len(self.fuzzyNumbers) < 1):
            return FuzzyNumber.ConstFuzzyNumber(0)
        
        fuzzyNumbersCount = len(self.fuzzyNumbers)
        values = np.zeros(fuzzyNumbersCount)
        for i, fuzzyNumber in enumerate(self.fuzzyNumbers):
            values[i] = fuzzyNumber.calculateValue(inputs[i])

        outValue = values[0]
        for i in range(1, fuzzyNumbersCount):
            outValue = self.operators[i - 1](outValue, values[i])
        outFuzzy = implication(self.outFuzzyNumber, FuzzyNumber.ConstFuzzyNumber(outValue))
        
        if(plotRanges is not None):
            for i, fuzzyNumber in enumerate(self.fuzzyNumbers):
                plt.subplot(plotRows, fuzzyNumbersCount + 1, (plotRow - 1) * (fuzzyNumbersCount + 1) + i + 1)
                fuzzyNumber.plotMe(plotRanges[i, 0], plotRanges[i, 1], plotRow == plotRows - 1)
                FuzzyNumber.MinFuzzyNumber(fuzzyNumber, FuzzyNumber.ConstFuzzyNumber(values[i])).plotMe(plotRanges[i, 0], plotRanges[i, 1], plotRow == plotRows - 1, True)
            
            plt.subplot(plotRows, fuzzyNumbersCount + 1, (plotRow - 1) * (fuzzyNumbersCount + 1) + fuzzyNumbersCount + 1)
            self.outFuzzyNumber.plotMe(plotRanges[-1, 0], plotRanges[-1, 1], False)
            outFuzzy.plotMe(plotRanges[-1, 0], plotRanges[-1, 1], False, True)
        
        return outFuzzy, outValue
class Ruleset:
    def __init__(self, rules):
        self.rules = rules
        
    def calculateValue(self, inputs, implication = FuzzyNumber.MinFuzzyNumber, aggregation = FuzzyNumber.MaxFuzzyNumber, plotRanges = None):
        rulesCount = len(self.rules)
        
        groupedValues = dict()
        for i, rule in enumerate(self.rules):
            _, value = rule.calculateValue(inputs, implication, plotRanges, rulesCount + 1, i + 1)
            if rule.outFuzzyNumber in groupedValues:
                groupedValues[rule.outFuzzyNumber] += value
            else:
                groupedValues[rule.outFuzzyNumber] = value

        outFuzzy = None
        for key, value in groupedValues.items():
            if(outFuzzy ==  None):
                outFuzzy = implication(key, FuzzyNumber.ConstFuzzyNumber(value))
            else:
                outFuzzy = aggregation(outFuzzy, implication(key, FuzzyNumber.ConstFuzzyNumber(value)))
        
        if plotRanges is not None:
            plt.subplot(rulesCount + 1, len(inputs) + 1, (rulesCount + 1) * (len(inputs) + 1))
            outFuzzy.plotMe(plotRanges[-1, 0], plotRanges[-1, 1])
            outFuzzy.plotMe(plotRanges[-1, 0], plotRanges[-1, 1], fill = True)
            cog = outFuzzy.centerOfGravity(np.arange(0, 10, 0.1))
            plt.plot([cog, cog], [0, 1], 'r')
        return outFuzzy
        

def prodOperator(a, b):
    return a * b
        
def proborOperator(a, b):
    return a + b - a * b