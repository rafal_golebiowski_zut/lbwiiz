import matplotlib.pyplot as plt
import numpy as np
import FuzzyNumber
import Rule


    
smallTemperature = FuzzyNumber.TrapezoidalFuzzyNumber(0, 60, 0, 45)
mediumTemperature = FuzzyNumber.TrapezoidalFuzzyNumber(45, 85, 60, 70)
largeTemperature = FuzzyNumber.TrapezoidalFuzzyNumber(60, 100, 80, 100)

smallCpuUsage = FuzzyNumber.TrapezoidalFuzzyNumber(0, 45, 0, 30)
mediumCpuUsage = FuzzyNumber.TrapezoidalFuzzyNumber(30, 80, 45, 60)
largeCpuUsage = FuzzyNumber.TrapezoidalFuzzyNumber(60, 100, 80, 100)

smallFanSpeed = FuzzyNumber.TrapezoidalFuzzyNumber(0, 2000, 0, 1200)
mediumFanSpeed = FuzzyNumber.TrapezoidalFuzzyNumber(1200, 2700, 2000, 2400)
largeFanSpeed = FuzzyNumber.TrapezoidalFuzzyNumber(2400, 3100, 2700, 3100)


r1 = Rule.Rule([smallTemperature, smallCpuUsage], [Rule.prodOperator], smallFanSpeed)
r2 = Rule.Rule([smallTemperature, mediumCpuUsage], [Rule.prodOperator], smallFanSpeed)
r3 = Rule.Rule([smallTemperature, largeCpuUsage], [Rule.prodOperator], mediumFanSpeed)
r4 = Rule.Rule([mediumTemperature, smallCpuUsage], [Rule.prodOperator], mediumFanSpeed)
r5 = Rule.Rule([mediumTemperature, mediumCpuUsage], [Rule.prodOperator], mediumFanSpeed)
r6 = Rule.Rule([mediumTemperature, largeCpuUsage], [Rule.prodOperator], largeFanSpeed)
r7 = Rule.Rule([largeTemperature, smallCpuUsage], [Rule.prodOperator], mediumFanSpeed)
r8 = Rule.Rule([largeTemperature, mediumCpuUsage], [Rule.prodOperator], largeFanSpeed)
r9 = Rule.Rule([largeTemperature, largeCpuUsage], [Rule.prodOperator], largeFanSpeed)

ruleset = Rule.Ruleset([r1, r2, r3, r4, r5, r6, r7, r8, r9])

out = ruleset.calculateValue([55, 75], plotRanges=np.array([[0, 100],
         [0, 100],
         [0, 3100]]))
print(out.centerOfGravity(np.arange(0, 3100, 0.1)))
plt.show()

