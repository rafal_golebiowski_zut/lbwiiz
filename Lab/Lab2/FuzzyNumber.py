import numpy as np
import matplotlib.pyplot as plt


class FuzzyNumber:
    def calculateValue(self, x):
        pass
    def plotMe(self, xmin, xmax, ticks = True, fill = False):
        X = np.linspace(xmin, xmax, num=(xmax-xmin) * 100 + 1)
        Y = list(map(self.calculateValue, X))
        if(fill == False):
            plt.plot(X, Y)
        else:
            plt.fill_between(X, Y)
        plt.ylim([-0.05, 1.05])
        plt.xlim([xmin - 0.05, xmax + 0.05])
        if ticks == False:
            plt.xticks([])
            plt.yticks([])
    #defuzzification
    def centerOfGravity(self, xRange):
        sum1 = 0
        sum2 = 0
        for x in xRange:
            tmp = self.calculateValue(x)
            sum1 += x * tmp
            sum2 += tmp
        return sum1/sum2

class TriangularFuzzyNumber(FuzzyNumber):
    def __init__(self, a, b, m):
        self.a = a
        self.b = b
        self.m = m
        
    def calculateValue(self, x):
        if(x <= self.a):
            return 0
        elif(x <= self.m):
            return (x - self.a)/(self.m - self.a)
        elif(x == self.m):
            return 1
        elif(x <= self.b):
            return (self.b - x)/(self.b - self.m)
        elif(x >= self.b):
            return 0
        
class TrapezoidalFuzzyNumber(FuzzyNumber):
    def __init__(self, a, b, mLeft, mRight):
        self.a = a
        self.b = b
        self.mLeft = mLeft
        self.mRight = mRight
        
    def calculateValue(self, x):
        if(x <= self.a):
            return 0
        elif(x <= self.mLeft):
            return (x - self.a)/(self.mLeft - self.a)
        elif(x <= self.mRight):
            return 1
        elif(x <= self.b):
            return (self.b - x)/(self.b - self.mRight)
        elif(x >= self.b):
            return 0

class SingletonFuzzyNumber(FuzzyNumber):
    def __init__(self, m):
        self.m = m
        
    def calculateValue(self, x):
        if(x == self.m):
            return 1
        return 0
    
class IntervalFuzzyNumber(FuzzyNumber):
    def __init__(self, a, b):
        self.a = a
        self.b = b
        
    def calculateValue(self, x):
        if(x >= self.a and x <= self.b):
            return 1
        return 0
    
class ProdFuzzyNumber(FuzzyNumber):
    def __init__(self, aFuzzy : FuzzyNumber, bFuzzy : FuzzyNumber):
        self.aFuzzy = aFuzzy
        self.bFuzzy = bFuzzy
        
    def calculateValue(self, x):
        return self.aFuzzy.calculateValue(x) * self.bFuzzy.calculateValue(x)
    
class SumFuzzyNumber(FuzzyNumber):
    def __init__(self, aFuzzy : FuzzyNumber, bFuzzy : FuzzyNumber):
        self.aFuzzy = aFuzzy
        self.bFuzzy = bFuzzy
        
    def calculateValue(self, x):
        a = self.aFuzzy.calculateValue(x)
        b = self.bFuzzy.calculateValue(x)
        return a + b - a * b
    
class MinFuzzyNumber(FuzzyNumber):
    def __init__(self, aFuzzy : FuzzyNumber, bFuzzy : FuzzyNumber):
        self.aFuzzy = aFuzzy
        self.bFuzzy = bFuzzy
        
    def calculateValue(self, x):
        return min(self.aFuzzy.calculateValue(x), self.bFuzzy.calculateValue(x))
    
class MaxFuzzyNumber(FuzzyNumber):
    def __init__(self, aFuzzy : FuzzyNumber, bFuzzy : FuzzyNumber):
        self.aFuzzy = aFuzzy
        self.bFuzzy = bFuzzy
        
    def calculateValue(self, x):
        return max(self.aFuzzy.calculateValue(x), self.bFuzzy.calculateValue(x))
  
class ConstFuzzyNumber(FuzzyNumber):
    def __init__(self, value):
        self.value = value
        
    def calculateValue(self, x):
        return self.value