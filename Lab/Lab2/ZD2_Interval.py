import matplotlib.pyplot as plt
import numpy as np
import FuzzyNumber
import Rule


    
smallDistance = FuzzyNumber.TriangularFuzzyNumber(0, 20, 0)
mediumDistance = FuzzyNumber.TriangularFuzzyNumber(0, 100, 20)
largeDistance = FuzzyNumber.TriangularFuzzyNumber(20, 100, 100)

smallAngle = FuzzyNumber.TriangularFuzzyNumber(0, 25, 0)
mediumAngle = FuzzyNumber.TriangularFuzzyNumber(0, 90, 25)
largeAngle = FuzzyNumber.TriangularFuzzyNumber(25, 90, 90)

verySmallVelocity = FuzzyNumber.IntervalFuzzyNumber(0, 0.5)
smallVelocity = FuzzyNumber.IntervalFuzzyNumber(0.5, 1.5)
mediumVelocity = FuzzyNumber.IntervalFuzzyNumber(1.5, 3)
largeVelocity = FuzzyNumber.IntervalFuzzyNumber(3, 7)
veryLargeVelocity = FuzzyNumber.IntervalFuzzyNumber(7, 10)


r1 = Rule.Rule([smallDistance, smallAngle], [Rule.prodOperator], verySmallVelocity)
r2 = Rule.Rule([smallDistance, mediumAngle], [Rule.prodOperator], verySmallVelocity)
r3 = Rule.Rule([smallDistance, largeAngle], [Rule.prodOperator], verySmallVelocity)
r4 = Rule.Rule([mediumDistance, smallAngle], [Rule.prodOperator], mediumVelocity)
r5 = Rule.Rule([mediumDistance, mediumAngle], [Rule.prodOperator], smallVelocity)
r6 = Rule.Rule([mediumDistance, largeAngle], [Rule.prodOperator], verySmallVelocity)
r7 = Rule.Rule([largeDistance, smallAngle], [Rule.prodOperator], veryLargeVelocity)
r8 = Rule.Rule([largeDistance, mediumAngle], [Rule.prodOperator], largeVelocity)
r9 = Rule.Rule([largeDistance, largeAngle], [Rule.prodOperator], mediumVelocity)

ruleset = Rule.Ruleset([r1, r2, r3, r4, r5, r6, r7, r8, r9])

out = ruleset.calculateValue([50, 10], plotRanges=np.array([[0, 100],
         [0, 90],
         [0, 10]]))
print(out.centerOfGravity(np.arange(0, 10, 0.1)))
plt.show()

