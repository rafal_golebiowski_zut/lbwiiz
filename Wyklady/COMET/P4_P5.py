import numpy as np

import FuzzyNumber
import CometRule
import CometUtils
import Comet

fuzzyNumberSet = CometUtils.loadFuzzyNumbersSets('P4_P5.csv')
MEJ = CometUtils.loadMEJ('P4_P5_MEJ.csv')

comet = Comet.Comet(fuzzyNumberSet)
comet.generateCharacteristicObjects()
comet.setMEJ(MEJ)
CometUtils.checkMEJ(MEJ)

comet.generateRuleset()

comet.ruleset.printRules()