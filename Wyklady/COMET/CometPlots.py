import numpy as np
import matplotlib.pyplot as plt

import FuzzyNumber
import CometRule
import CometUtils
import Comet


fuzzyNumberSets = CometUtils.loadFuzzyNumbersSets(f'CometPlots.csv')
plt.rcParams["figure.figsize"] = (12,16)
for i, fuzzyNumberSet in enumerate(fuzzyNumberSets):
    ax = plt.subplot(4, 2, i + 1)
    xMin = min(fuzzyNumberSet.fuzzyNumbers, key = lambda x: x.a).a
    xMax = max(fuzzyNumberSet.fuzzyNumbers, key = lambda x: x.b).b
    for fuzzyNumber in fuzzyNumberSet.fuzzyNumbers:
        ax.plot([xMin, fuzzyNumber.a, fuzzyNumber.m, fuzzyNumber.b, xMax], [0, 0, 1, 0, 0])
    ax.set_title(fuzzyNumberSet.name)
plt.savefig("CometPlots.png", dpi = 300)
