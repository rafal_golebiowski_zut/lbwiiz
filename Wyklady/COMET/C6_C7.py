import numpy as np

import FuzzyNumber
import CometRule
import CometUtils
import Comet

fuzzyNumberSet = CometUtils.loadFuzzyNumbersSets('C6_C7.csv')
MEJ = CometUtils.loadMEJ('C6_C7_MEJ.csv')

comet = Comet.Comet(fuzzyNumberSet)
comet.generateCharacteristicObjects()
comet.setMEJ(MEJ)
CometUtils.checkMEJ(MEJ)
CometUtils.plotMEJ(MEJ)

comet.generateRuleset()

comet.ruleset.printRules()