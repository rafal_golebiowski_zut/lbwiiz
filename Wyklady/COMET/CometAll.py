import numpy as np

import FuzzyNumber
import CometRule
import CometUtils
import Comet



comets = []
for tmp in ['C1_C2', 'P1_C3', 'C4_C5', 'C6_C7', 'P2_P3', 'P4_P5']: 
    fuzzyNumberSet = CometUtils.loadFuzzyNumbersSets(f'{tmp}.csv')
    MEJ = CometUtils.loadMEJ(f'{tmp}_MEJ.csv')

    comet = Comet.Comet(fuzzyNumberSet)
    comet.generateCharacteristicObjects()
    comet.setMEJ(MEJ)
    CometUtils.checkMEJ(MEJ)

    comet.generateRuleset()
    comet.ruleset.printRules()
    comets.append(comet)

cometInputs = []
cometInputs.append(np.array([21000, 9800, 2500, 167.5, 0, 0, 0]))
cometInputs.append(np.array([21000, 10100, 4000, 161.2, 3, 3, 2]))
cometInputs.append(np.array([21000, 11400, 7000, 83.8, 3, 3, 2]))
cometInputs.append(np.array([86000, 22000, 20000, 95.4, 1, 1, 1]))
cometInputs.append(np.array([29000, 15000, 4000, 158.2, 1, 0, 0]))
cometInputs.append(np.array([59000, 15000, 7000, 105.8, 1, 0, 0]))
cometInputs.append(np.array([59000, 15000, 17500, 72.9, 1, 1, 1]))


for i, cometInput in enumerate(cometInputs):
    P1 = comets[0].calculateValue(np.array([cometInput[0], cometInput[1]]))
    P2 = comets[2].calculateValue(np.array([cometInput[3], cometInput[4]]))
    P3 = comets[3].calculateValue(np.array([cometInput[5], cometInput[6]]))
    P4 = comets[1].calculateValue(np.array([P1, cometInput[2]]))
    P5 = comets[4].calculateValue(np.array([P2, P3]))
    P = comets[5].calculateValue(np.array([P4, P5]))
    print(f'A{i+1}: {P}')
    print(f'\tP1: {P1}')
    print(f'\tP2: {P2}')
    print(f'\tP3: {P3}')
    print(f'\tP4: {P4}')
    print(f'\tP5: {P5}')


