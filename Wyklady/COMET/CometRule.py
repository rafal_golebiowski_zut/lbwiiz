import numpy as np
import FuzzyNumber

class Rule:
    def __init__(self, fuzzyNumbers, outFuzzyNumber : FuzzyNumber.SingletonFuzzyNumber):
        self.fuzzyNumbers = fuzzyNumbers
        self.outFuzzyNumber = outFuzzyNumber
        
    def calculateValue(self, inputs):
        if(len(self.fuzzyNumbers) < 1):
            return 0
        
        fuzzyNumbersCount = len(self.fuzzyNumbers)
        values = np.zeros(fuzzyNumbersCount)
        for i, fuzzyNumber in enumerate(self.fuzzyNumbers):
            values[i] = fuzzyNumber.calculateValue(inputs[i])

        outValue = values[0]
        for i in range(1, fuzzyNumbersCount):
            outValue *= values[i]
        
        return outValue
    
    def printMe(self):
        print("IF", end = " ")
        for i, fuzzyNumber in enumerate(self.fuzzyNumbers):
            print(f'C{i} ~ {fuzzyNumber.m} AND ', end = "")

        print("\b\b\b\bTHEN P ~", self.outFuzzyNumber.m)

class Ruleset:
    def __init__(self, rules):
        self.rules = rules
        
    def calculateValue(self, inputs):
        groupedValues = dict()
        for _, rule in enumerate(self.rules):
            value = rule.calculateValue(inputs)
            if rule.outFuzzyNumber in groupedValues:
                groupedValues[rule.outFuzzyNumber] += value
            else:
                groupedValues[rule.outFuzzyNumber] = value

        out = None
        for key, value in groupedValues.items():
            if(out ==  None):
                out = key.m * value
            else:
                out = out + key.m * value
        return out
    
    def printRules(self):
        for i, rule in enumerate(self.rules):
            print(f'R{i + 1}:', end = " ")
            rule.printMe()
        