import numpy as np

import FuzzyNumber
import CometRule
import CometUtils
import Comet

fuzzyNumberSet = CometUtils.loadFuzzyNumbersSets('P2_P3.csv')
MEJ = CometUtils.loadMEJ('P2_P3_MEJ.csv')

comet = Comet.Comet(fuzzyNumberSet)
comet.generateCharacteristicObjects()
comet.setMEJ(MEJ)
CometUtils.checkMEJ(MEJ)

comet.generateRuleset()

comet.ruleset.printRules()